﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SImpleCodeTest.Models;

namespace SImpleCodeTest.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VisitsController : ControllerBase
    {
        ApplicationContext db;
        public VisitsController(ApplicationContext context)
        {
            db = context; 

        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Visit>>> Get()
        {
            return await db.Visits.ToListAsync();
        } 

        // GET api/patients/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Visit>> Get(int id)
        {
            Visit visit = await db.Visits.FirstOrDefaultAsync(x => x.VisitId == id);
            if (visit == null)
                //"title": "Not Found",
                //"status": 404,
                return NotFound();
            return new ObjectResult(visit);
        }

        // POST api/users
        [HttpPost]
        public async Task<ActionResult<Visit>> Post(Visit visit)
        {
            if (visit == null)
            {
                //"title": "Bad Request",
                //"status": 400
                return BadRequest();
            }

            db.Visits.Add(visit);
            await db.SaveChangesAsync();
            return Ok(visit);
        }

        // PUT api/users/
        [HttpPut]
        public async Task<ActionResult<Doctor>> Put(Visit visit)
        {
            if (visit == null)
            {
                return BadRequest();
            }
            if (!db.Visits.Any(x => x.VisitId == visit.VisitId))
            {    //"title": "Not Found",
                //"status": 404,
                return NotFound();
            }

            db.Visits.Update(visit);
            await db.SaveChangesAsync();
            return Ok(visit);
        }
        // DELETE api/users/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Patient>> Delete(int id)
        {
            Visit visit = db.Visits.FirstOrDefault(x => x.VisitId == id);
            if (visit == null)
            {
                //"title": "Not Found",
                //"status": 404,

                return NotFound();
            }
            db.Visits.Remove(visit);
            await db.SaveChangesAsync();
            return Ok(visit);
        }

    }
}
