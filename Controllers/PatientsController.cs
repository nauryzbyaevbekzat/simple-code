﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SImpleCodeTest.Models;

namespace SImpleCodeTest.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PatientsController : ControllerBase
    {
        ApplicationContext db;
        public PatientsController(ApplicationContext context)
        {
            db = context;
         
        }
        // GET api/patients/FindByIIN/?FindByIIN=Your IIN
        [HttpGet]
        [Route("[action]", Name = "FindByIIN")]
        public async Task<ActionResult<Patient>> FindByIIN(String IIN) 
        {   
            var select = await ( from patient in db.Patients 
                         join visit in db.Visits on patient.PatientId equals visit.PatientId
                         join doctor in db.Doctors on visit.DoctorId equals doctor.DoctorId
                          select new 
                         {
                            IIN = patient.IIN ,
                            Complaints = visit.Complaints,
                            Diagnosis  = visit.Diagnosis,
                            VisitDate = visit.VisitDate,
                            DoctorId = doctor.DoctorId,
                            Speciality = doctor.Speciality,
                         }).Where(p => p.IIN == IIN).ToListAsync();
          
            if (select == null)
                //"title": "Not Found",
                //"status": 404,
                return NotFound();
            return new ObjectResult(select);
        }
        // GET api/patients/FindByName/?FirstName=blabla&Lastname=blabla
        [HttpGet]
        [Route("[action]", Name = "FindByName")]
        public async Task<ActionResult<Patient>> FindByName(String FirstName , String Lastname)
        {
          
            var select = await (from patient in db.Patients
                                              where patient.FirstName == FirstName && patient.LastName == Lastname
                                              select patient).ToListAsync(); 
            if (select == null)
                //"title": "Not Found",
                //"status": 404,
                return NotFound();
            return new ObjectResult(select);
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<Patient>>> Get()
        {
            return await db.Patients.ToListAsync();
        }

        // GET api/patients/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Patient>> Get(int id)
        {
            Patient patient = await db.Patients.FirstOrDefaultAsync(x => x.PatientId == id);
            if (patient == null)
                //"title": "Not Found",
                //"status": 404,
                return NotFound();
            return new ObjectResult(patient);
        }

        // POST api/patients
        [HttpPost]
        public async Task<ActionResult<Patient>> Post(Patient patient)
        { 
            if (patient == null)
            {
                //"title": "Bad Request",
                //"status": 400
                return BadRequest();
            }

            db.Patients.Add(patient);
            await db.SaveChangesAsync();
            return Ok(patient);
        }

        // PUT api/patients/
        [HttpPut]
        public async Task<ActionResult<Patient>> Put(Patient patient)
        {
            if (patient == null)
            {
                //"title": "Bad Request",
                //"status": 400
                return BadRequest();
            }
            if (!db.Patients.Any(x => x.PatientId == patient.PatientId))
            {    //"title": "Not Found",
                //"status": 404,
                return NotFound();
            }

            db.Patients.Update(patient);
            await db.SaveChangesAsync();
            return Ok(patient);
        }
        // DELETE api/patients/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Patient>> Delete(int id)
        {
            Patient patient = db.Patients.FirstOrDefault(x => x.PatientId == id);
            if (patient == null)
            {    //"title": "Not Found",
                //"status": 404,
                return NotFound();
            }
            db.Patients.Remove(patient);
            await db.SaveChangesAsync();
            return Ok(patient);
        }



    }
}
