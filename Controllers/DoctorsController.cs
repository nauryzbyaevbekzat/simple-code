﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SImpleCodeTest.Models;

namespace SImpleCodeTest.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DoctorsController : ControllerBase
    {
        ApplicationContext db;
        public DoctorsController(ApplicationContext context)
        { 
            db = context;

        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Doctor>>> Get()
        {
            return await db.Doctors.ToListAsync();
        }

        // GET api/doctors/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Patient>> Get(int id)
        {
            Doctor doctor = await db.Doctors.FirstOrDefaultAsync(x => x.DoctorId == id);
            if (doctor == null)
                //"title": "Not Found",
                //"status": 404
                return NotFound();
            return new ObjectResult(doctor);
        }

        // POST api/doctors
        [HttpPost]
        public async Task<ActionResult<Doctor>> Post(Doctor doctor)
        {
            if (doctor == null)
            {
                
                //"title": "Bad Request",
                //"status": 400
                return BadRequest();
            }

            db.Doctors.Add(doctor);
            await db.SaveChangesAsync();
            return Ok(doctor);
        }

        // PUT api/doctors/
        [HttpPut]
        public async Task<ActionResult<Doctor>> Put(Doctor doctor)
        {
            if (doctor == null)
            {
                //"title": "Bad Request",
                //"status": 400
                return BadRequest();
            }
            if (!db.Doctors.Any(x => x.DoctorId == doctor.DoctorId))
            {    //"title": "Not Found",
                //"status": 404,
                return NotFound();
            }

            db.Doctors.Update(doctor);
            await db.SaveChangesAsync();
            return Ok(doctor);
        }
        // DELETE api/doctors/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Patient>> Delete(int id)
        {
            Doctor doctor= db.Doctors.FirstOrDefault(x => x.DoctorId == id);
            if (doctor == null)
            {    //"title": "Not Found",
                //"status": 404,
                return NotFound();
            }
            db.Doctors.Remove(doctor);
            await db.SaveChangesAsync();
            return Ok(doctor);
        }

    }
}
