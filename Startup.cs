using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SImpleCodeTest.Models;

namespace SImpleCodeTest
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {   //The connection string is in the file appsettings.json , 
            // get the connection string from the config file
            string connection = Configuration.GetConnectionString("DefaultConnection");
            // add the ApplicationContext as a service to the application
            
            services.AddDbContext<ApplicationContext>(options =>
                options.UseSqlServer(connection));
            //Web API controllers required for the operation are performed using the services.AddControllers() method
            services.AddControllers();

            services.AddSwaggerGen(options => {

                options.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "Swagger SimpleCode Test API",
                    Description = "Test API",
                    Version = "v1"

                });
            }
            );


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {   // if the application is under development
            if (env.IsDevelopment())
            {   // then display information about the error, if there is an error
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            // add routing capabilities
            app.UseRouting();

            app.UseAuthorization();
            // set the addresses to be processed
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();// no routes defined
            });

            //API documentation
            //https://localhost:44326/swagger/index.html 
            app.UseSwagger();
            app.UseSwaggerUI(

                options => {
                    options.SwaggerEndpoint("/swagger/v1/swagger.json", "Test API");
                }
            );
        }
    }
}
