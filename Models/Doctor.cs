﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SImpleCodeTest.Models
{
    public class Doctor
    {   //primary key 
        public int DoctorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Speciality { get; set; }

        public List<Visit> Visits { get; set; }
    }
}
