﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SImpleCodeTest.Models
{   //To interact with the database through the Entity Framework, 
    //we need a data context - a class inherited from the Microsoft.EntityFrameworkCore.DbContext class.
    public class ApplicationContext : DbContext
    {
        public DbSet<Visit> Visits{ get; set; }
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Patient> Patients { get; set; } 
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();   // create database on first posting
        }
    }
    //The project consists of three tables (Visits , Doctors , Patients)
    //Relationship between tables (Patients ,Visits) one to many
    //Relationship between tables (Doctors , Visits) one to many
}
