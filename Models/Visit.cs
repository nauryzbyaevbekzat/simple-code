﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SImpleCodeTest.Models
{
    public class Visit
    {   //primary key 
        public int VisitId { get; set; } 
        public string Diagnosis { get; set; }
        public string Complaints  { get; set; }
        public DateTime VisitDate { get; set; }

        //foreign key 
        //navigation property
        public int PatientId { get; set; }
        public Patient Patient { get; set; }


        //foreign key
        //navigation property
        public int DoctorId { get; set; }
        public Doctor Doctor { get; set; }
    }
}
